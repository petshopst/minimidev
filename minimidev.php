<?php
/**
* 2007-2022 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2022 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class MinimiDev extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'minimidev';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Klever';
        $this->need_instance = 1;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Minimi');
        $this->description = $this->l('Modulo para editar el template para minimi');
        $this->confirmUninstall = $this->l('Estas seguro que quieres desinstalarme ?');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }


    public function install()
    {
      //  Configuration::updateValue('MINIMIDEV_LIVE_MODE', false);
     //  include(dirname(__FILE__).'/sql/install.php');
      return parent::install() &&
      $this->registerHook('displayHeader') &&
      $this->registerHook('displayContentWrapperTop') &&
      $this->registerHook('displayHeaderCategory') &&
      $this->registerHook('actionCustomerAccountAdd');

    }

    public function uninstall()
    {
        //Configuration::deleteByName('MINIMIDEV_LIVE_MODE');
        //include(dirname(__FILE__).'/sql/uninstall.php');
       return parent::uninstall();
    }





    public function HookActionCustomerAccountAdd()
{
    //Tools::redirect('index.php?controller=identity');
}



    public function HookDisplayHeader()
    {


        $this->context->controller->registerStylesheet('modules-minimidev-slick-theme', 'modules/'.$this->name.'/css/slick-theme.css', ['media' => 'all', 'priority' => 160]);
        $this->context->controller->registerStylesheet('modules-minimidev-slick', 'modules/'.$this->name.'/css/slick.css', ['media' => 'all', 'priority' => 160]);
        $this->context->controller->registerJavascript('modules-minimidev-slick', 'modules/'.$this->name.'/js/slick.js', ['position' => 'top', 'priority' => 160]);
        $this->context->controller->registerStylesheet('modules-minimidev', 'modules/'.$this->name.'/css/video.css', ['media' => 'all', 'priority' => 150]);
        $this->context->controller->registerJavascript('modules-minimidev', 'modules/'.$this->name.'/js/video.js', ['position' => 'top', 'priority' => 150]);
    }



    public function HookDisplayHeaderCategory()
    {

$id_parent_category = (int)Tools::getValue('id_category');

$categoryObj = new Category($id_parent_category);

if (Validate::isLoadedObject($categoryObj)) {
	$categoryList = $categoryObj->getSubCategories($this->context->language->id);
echo '<div class="megacategorias_petshop">';
	if ($categoryList && count($categoryList) > 0) {
		foreach ($categoryList AS $key => $val) {
    	$id_category = $val['id_category'];
      $name = $val['name'];
      echo '<div class="megacategorias_petshop_content"><div class="cat_petshop">';
      echo '<div class="cat_petshop_top">';
      echo '<h2>'.$name.'</h2><span><a href="#">Mostrar todo</a></span>';
      echo '</div>';
      $description = $val['description'];
      $subcategoryObj = new Category($id_category);
      $subcategoryList = $subcategoryObj->getSubCategories($this->context->language->id);
      echo '<div class="cat_petshop_content">';
      if ($subcategoryList && count($subcategoryList) > 0) {
        	foreach ($subcategoryList AS $key => $subval) {
            $sub_id_category = $subval['id_category'];
            $subname = $subval['name'];
            $link = new Link;
            $caturl =  $this->context->link->getCategoryLink($sub_id_category);
            $imgurl =  $this->context->link->getCatImageLink($subname, $subval['id_image'], 'small_default');

            echo  '<a class="cat_petshop_box_link" href="'.$caturl.'"><div class="cat_petshop_box">';
            echo '<img class="previo_cat" src="'.$imgurl.'" alt="">';
            echo '<h6>'.$subname.'</h6>';
            echo  '</div></a>';
         }
       }
      echo '</div></div></div>';
		}
	}
  echo '</div>';
}
    }




    public function getContent()
    {
      return $this->postProcess() . $this->getForm() . $this->getFormSuscripcion();
    }

    private function postProcess()
    {
            if (Tools::isSubmit('actualizar_home')) {
              $hero_titulo_content = Tools::getValue('hero_titulo_content');
              Configuration::updateValue('MINIMI_MODULE_TITULO_HERO_CONTENT',$hero_titulo_content);
              $hero_desc_content = Tools::getValue('hero_desc_content');
              Configuration::updateValue('MINIMI_MODULE_DESC_HERO_CONTENT',$hero_desc_content);
            }
    }


   private function getForm()
   {

     $helper = new HelperForm();
     $helper->module = $this;
     $helper->name_controller = $this->name;
     $helper->identifier = $this->identifier;
     $helper->token = Tools::getAdminTokenLite('AdminModules');
     $helper->languages = $this->context->controller->getLanguages();
     $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
     $helper->default_form_language = $this->context->controller->default_form_language;
     $helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;
     $helper->title = $this->displayName;
     $helper->submit_action = 'actualizar_home';

     $helper->fields_value = [
         'hero_titulo_content' => Configuration::get('MINIMI_MODULE_TITULO_HERO_CONTENT'),
         'hero_desc_content' => Configuration::get('MINIMI_MODULE_DESC_HERO_CONTENT'),
     ];

     $form[] = [
         'form' => [
             'legend' => [
                 'title' => $this->l('Editar Home'),
                 'icon' => 'icon-cogs',
             ],
             'input' => [
                 [
                     'type' => 'text',
                     'name' => 'hero_titulo_content',
                     'label' => $this->l('Editar Titulo de Hero principal'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'hero_desc_content',
                     'label' => $this->l('Editar Descripcion de Hero principal'),

                 ],
               /*
                 [
                     'type' => 'text',
                     'name' => 'linkperro_content',
                     'label' => $this->l('Editar Link Perro'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'linkgato_content',
                     'label' => $this->l('Editar Link Gato'),

                 ],
                 [
                     'type' => 'textarea',
                     'name' => 'perrogato_content',
                     'label' => $this->l('Editar Texto de Perro o Gato'),

                 ],
                 [
                     'type' => 'file',
                     'name' => 'video_content',
                     'label' => $this->l('Video principal'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'video_mobil_content',
                     'label' => $this->l('Video para mobil (Link de Youtube)'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'video_titulo_content',
                     'label' => $this->l('Titulo para Video'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'video_desc_content',
                     'label' => $this->l('Descripcion para Video'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'titulo_amigosminimi_content',
                     'label' => $this->l('Titulo sobre amigos minimi'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'desc_amigosminimi_content',
                     'label' => $this->l('Descripcion sobre amigos minimi'),

                 ],

                 [
                     'type' => 'text',
                     'name' => 'desc_orfertas_content',
                     'label' => $this->l('Descripcion Ofertas y novedades'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'titulo_orfertas_content',
                     'label' => $this->l('Titulo Ofertas y novedades'),

                 ],

                 */
             ],
             'submit' => [
                 'title' => $this->l('Actualizar'),
                 'class' => 'btn btn-default pull-right',
             ],
           ],
        ];

  return  $helper->generateForm($form);


   }


   private function getFormSuscripcion()
   {

     $helper = new HelperForm();
     $helper->module = $this;
     $helper->name_controller = $this->name;
     $helper->identifier = $this->identifier;
     $helper->token = Tools::getAdminTokenLite('AdminModules');
     $helper->languages = $this->context->controller->getLanguages();
     $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
     $helper->default_form_language = $this->context->controller->default_form_language;
     $helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;
     $helper->title = $this->displayName;
     $helper->submit_action = 'actualizar_suscripcion';

     $helper->fields_value = [
         'hero_titulo_content' => Configuration::get('MINIMI_MODULE_TITULO_HERO_CONTENT'),
         'hero_desc_content' => Configuration::get('MINIMI_MODULE_DESC_HERO_CONTENT'),
     ];

     $form[] = [
         'form' => [
             'legend' => [
                 'title' => $this->l('Editar Suscripcion'),
                 'icon' => 'icon-cogs',
             ],
             'input' => [
                 [
                     'type' => 'text',
                     'name' => 'hero_titulo_content',
                     'label' => $this->l('Editar Titulo de Hero principal'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'hero_desc_content',
                     'label' => $this->l('Editar Descripcion de Hero principal'),

                 ],
               /*
                 [
                     'type' => 'text',
                     'name' => 'linkperro_content',
                     'label' => $this->l('Editar Link Perro'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'linkgato_content',
                     'label' => $this->l('Editar Link Gato'),

                 ],
                 [
                     'type' => 'textarea',
                     'name' => 'perrogato_content',
                     'label' => $this->l('Editar Texto de Perro o Gato'),

                 ],
                 [
                     'type' => 'file',
                     'name' => 'video_content',
                     'label' => $this->l('Video principal'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'video_mobil_content',
                     'label' => $this->l('Video para mobil (Link de Youtube)'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'video_titulo_content',
                     'label' => $this->l('Titulo para Video'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'video_desc_content',
                     'label' => $this->l('Descripcion para Video'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'titulo_amigosminimi_content',
                     'label' => $this->l('Titulo sobre amigos minimi'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'desc_amigosminimi_content',
                     'label' => $this->l('Descripcion sobre amigos minimi'),

                 ],

                 [
                     'type' => 'text',
                     'name' => 'desc_orfertas_content',
                     'label' => $this->l('Descripcion Ofertas y novedades'),

                 ],
                 [
                     'type' => 'text',
                     'name' => 'titulo_orfertas_content',
                     'label' => $this->l('Titulo Ofertas y novedades'),

                 ],

                 */
             ],
             'submit' => [
                 'title' => $this->l('Actualizar'),
                 'class' => 'btn btn-default pull-right',
             ],
           ],
        ];

  return  $helper->generateForm($form);


   }

   public function HookDisplayContentWrapperTop()
   {
     $titulo_hero_content = Configuration::get('MINIMI_MODULE_TITULO_HERO_CONTENT');
         $this->context->smarty->assign([
             'titulo_hero_content' => $titulo_hero_content,
           ]);

         return $this->display(__FILE__,'home.tpl');
   }


/*

    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMinimidevModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }


    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'MINIMIDEV_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'MINIMIDEV_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'MINIMIDEV_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }


    protected function getConfigFormValues()
    {
        return array(
            'MINIMIDEV_LIVE_MODE' => Configuration::get('MINIMIDEV_LIVE_MODE', true),
            'MINIMIDEV_ACCOUNT_EMAIL' => Configuration::get('MINIMIDEV_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'MINIMIDEV_ACCOUNT_PASSWORD' => Configuration::get('MINIMIDEV_ACCOUNT_PASSWORD', null),
        );
    }


    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }


    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayBanner()
    {

    }
  */

}
